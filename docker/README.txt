Commands to run sample code in Docker:

```
docker build --rm --force-rm -t dsp/assignment-3 . 
docker image list
docker run dsp/assignment-3 
```
